import {Route, Navigate} from "react-router-dom";
import { RootStateOrAny, useDispatch } from "react-redux";
import {FC, useEffect, useState} from "react";
import * as React from "react";
import {setRole} from "./action/setRole";
import Loader from "./Loader";
import ForbiddenAccess from "./ForbiddenAccess";
import UserService from "../service/user-service";

interface  IProps {
    role?: string[];
    element: JSX.Element
}

const Auth: FC<IProps> = ({role = [], element}) => {
    type User = {
        name: string;
        email: string;
        role : string;
    }
    const [loggedInUser, setLoggedInUser] = useState({} as User);
    const [spinning, setSpinning] = useState<boolean>(true);

    const dispatch = useDispatch();

    useEffect(()=>
    {   
        UserService.fetchLoggedInUserDetails()
        .then((user) => {
            setLoggedInUser(user);
            dispatch(setRole(user.role));
            setSpinning(false);
        }).catch((err)=> {
            setSpinning(false);
        });

    },[])

    if(spinning)
        return <Loader/>;
    else if(role.length===0 || role.includes(loggedInUser.role))
        return element;
    else if(role.length!==0 && !role.includes(loggedInUser.role))
        return <ForbiddenAccess />;
    /**
     Case 1, If no role is passed, then all the logged In user can access this path
     Example: role=[] or role=undefined
     Case 2, If role is passed, only loggedIn user whose role is defined in passed roles array can access this path
     Example role=['role1', 'role2', 'role3'] an loggedInUser.role === 'role1'
     */


    /**
     * Case 3, if roles array does not contain the role of logged in user, show Forbidden Access component
     * Example role=['role1', 'role2', 'role3'] an loggedInUser.role === 'role4'
     */


    /**
     * Redirect to Login page if unauthorised
     */
    return <Navigate to="/login" replace={true}/>

}

export default Auth;