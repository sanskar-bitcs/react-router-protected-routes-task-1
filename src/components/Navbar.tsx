import {NavLink} from "react-router-dom";
import "../App.css";
import { RootStateOrAny, useSelector } from "react-redux";

const Navbar = () => {
    const role = useSelector((state: RootStateOrAny) => state.role);
    console.log(role);
    return (
        <ul>
            <li><NavLink className="Nav" to="/">Main Page</NavLink></li>
            {(role!=='admin') ? '' : <> <li><NavLink className="Nav" to="/dashboard">Dashboard Page</NavLink></li> </>}
            <li><NavLink className="Nav" to="/home">Home Page</NavLink></li>
            <li><NavLink className="Nav" to="/login">Login Page</NavLink></li>
        </ul>
    )
}

export default Navbar;