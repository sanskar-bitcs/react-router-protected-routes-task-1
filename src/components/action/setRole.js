export const setRole = (payload) => {
    return {
        type: 'setRole',
        payload : payload,
    };
};
