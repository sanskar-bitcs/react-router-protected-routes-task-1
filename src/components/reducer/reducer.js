const initState = {
    role: '',
};

export const reducer = (initialState = initState, action) =>
{
    switch(action.type)
    {
        case 'setRole':
            return {...initialState, role: action.payload}
        default:
            return initialState;
    }
}